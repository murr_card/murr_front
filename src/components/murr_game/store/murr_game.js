export default {
  state: {
    murrBattleRoomList: [],
    murrBattleRoomPlayersData: null,
    showCreateMurrBattleRoomBtn: true,
  },
  mutations: {
    setMurrBattleRoomList_mutations(state, murrBattleRoomData) {
      const data = patchMurrenInRoomGroup(murrBattleRoomData);
      state.murrBattleRoomList.push(data);
    },
    updateRoomStateInMurrBattleLobby(state, murrBattleRoomMurrensList) {
      const data = patchMurrenInRoomGroup(murrBattleRoomMurrensList);
      const findRoom = (l) => l.room_id === data.room_id;
      const indexRoom = state.murrBattleRoomList.findIndex(findRoom);
      const room = state.murrBattleRoomList[indexRoom];
      room.room_groups = data.room_groups;
      state.murrBattleRoomList[indexRoom] = room;
    },
    popMurrBattleRoom_mutations(state, room_id) {
      state.murrBattleRoomList.forEach((room, index) => {
        if (room.room_id === room_id) {
          state.murrBattleRoomList.splice(index, 1);
        }
      });
    },
    popMurrenFromBattleRoomList(state, murren_id) {
      state.murrBattleRoomList.forEach((room) => {
        room.room_groups.first_group.members.forEach((m, index) => {
          if (m.murren_id === murren_id) {
            room.room_groups.first_group.members.splice(index, 1);
            room.room_groups.first_group.members.push({
              murren_id: Math.random().toString(36).substring(7),
            });
          }
        });
        room.room_groups.second_group.members.forEach((m, index) => {
          if (m.murren_id === murren_id) {
            room.room_groups.second_group.members.splice(index, 1);
            room.room_groups.second_group.members.push({
              murren_id: Math.random().toString(36).substring(7),
            });
          }
        });
      });
    },
    setShowCreateMurrBattleRoomBtn_mutations(state, show) {
      state.showCreateMurrBattleRoomBtn = show;
    },
  },
  getters: {
    getMurrBattleRoomList_getters(state) {
      return [
        ...new Map(
          state.murrBattleRoomList.map((item) => [item.room_name, item])
        ).values(),
      ];
    },
    getShowCreateMurrBattleRoomBtn_getters(state) {
      return state.showCreateMurrBattleRoomBtn;
    },
  },
};

function patchMurrenInRoomGroup(murrBattleRoomData) {
  if (
    murrBattleRoomData.room_groups.first_group.members.length <
    murrBattleRoomData.room_groups.first_group.group_len
  ) {
    const count =
      murrBattleRoomData.room_groups.first_group.group_len -
      murrBattleRoomData.room_groups.first_group.members.length;
    for (let i = 0; i < count; i++) {
      murrBattleRoomData.room_groups.first_group.members.push({
        murren_id: Math.random().toString(36).substring(7),
      });
    }
  }
  if (
    murrBattleRoomData.room_groups.second_group.members.length <
    murrBattleRoomData.room_groups.second_group.group_len
  ) {
    const count =
      murrBattleRoomData.room_groups.second_group.group_len -
      murrBattleRoomData.room_groups.second_group.members.length;
    for (let i = 0; i < count; i++) {
      murrBattleRoomData.room_groups.second_group.members.push({
        murren_id: Math.random().toString(36).substring(7),
      });
    }
  }
  return murrBattleRoomData;
}

export function getRoomMembersIds(roomGroups) {
  const groupMembersIds = [];
  const toObj = JSON.parse(JSON.stringify(roomGroups));
  const toList = Object.values(toObj);
  toList.forEach((group) => {
    group.members.forEach((member) => {
      groupMembersIds.push(member.murren_id);
    });
  });
  return groupMembersIds;
}
