import axios from "axios";
const CancelToken = axios.CancelToken;
let axiosCancelRequest;

import {
  STATUS_200_OK,
  STATUS_401_UNAUTHORIZED,
  STATUS_204_NO_CONTENT,
} from "../../../utils/http_response_status.js";
import { handlerErrorAxios } from "../../../utils/helpres.js";

import {
  MURREN_AVATAR_RESIZE,
  MURREN_AVATAR_UPDATE,
  MURREN_AVATAR_CANCEL_UPLOAD,
  MURREN_UPDATE_PROFILE,
  SWITCH_MURR_CARD_NOTIFICATIONS,
  SWITCH_SUBSCRIPTION_NOTIFICATIONS,
  MURREN_FETCH_PROFILE,
  MURREN_SUBSCRIBE,
  MURREN_PROFILE_SET,
  MURREN_PROFILE_CHANGE_SUBSCRIBER_STATUS,
} from "./type.js";

export default {
  async [MURREN_FETCH_PROFILE]({ commit }, payload) {
    try {
      const username = payload.username;

      const { status, data } = await axios.get(`/api/murren/${username}/`);

      let result = { success: true, data, status };

      if (status === STATUS_401_UNAUTHORIZED) {
        return { ...result, success: false };
      }

      if (status === STATUS_200_OK) {
        commit(MURREN_PROFILE_SET, data);
        return result;
      }

      return { ...result, success: false };
    } catch (error) {
      return handlerErrorAxios(error);
    }
  },

  async [MURREN_UPDATE_PROFILE]({ commit, getters }, payload) {
    try {
      const username = getters.currentMurren.username;

      const { status, data } = await axios.patch(
        `/api/murren/${username}/`,
        payload
      );

      let result = { success: true, data, status };

      if (status === STATUS_401_UNAUTHORIZED) {
        return { ...result, success: false };
      }

      if (status === STATUS_200_OK) {
        commit(MURREN_PROFILE_SET, data);
        return result;
      }

      return { ...result, success: false };
    } catch (error) {
      return handlerErrorAxios(error);
    }
  },

  async [MURREN_AVATAR_RESIZE](_, { avatar }) {
    try {
      const formData = new FormData();
      /* tmp_image_name.jpeg - PIL requires the name of the file with the extension. Not used anywhere else */
      formData.append("avatar", avatar, "tmp_image_name.jpeg");

      const { status, data } = await axios.post(
        "/api/murren/avatar_resize/",
        formData,
        {
          responseType: "blob",
          headers: {
            "Content-Type": "multipart/form-data",
          },
          cancelToken: new CancelToken((c) => (axiosCancelRequest = c)),
        }
      );

      let result = { success: true, data, status };

      if (status === STATUS_401_UNAUTHORIZED) {
        return { ...result, success: false };
      }

      if (status === STATUS_200_OK) {
        return result;
      }

      axiosCancelRequest = null;

      return { ...result, success: false };
    } catch (error) {
      return handlerErrorAxios(error);
    }
  },

  async [MURREN_AVATAR_UPDATE](_, { avatar, crop }) {
    try {
      const formData = new FormData();
      /* tmp_image_name.jpeg - PIL requires the name of the file with the extension. Not used anywhere else */
      formData.append("avatar", avatar, "tmp_image_name.jpeg");
      formData.append("crop.x", Math.round(crop.x));
      formData.append("crop.y", Math.round(crop.y));
      formData.append("crop.width", Math.round(crop.width));
      formData.append("crop.height", Math.round(crop.height));

      const { status, data } = await axios.patch(
        "/api/murren/avatar_update/",
        formData,
        {
          headers: {
            "Content-Type": "multipart/form-data",
          },
          cancelToken: new CancelToken((c) => (axiosCancelRequest = c)),
        }
      );

      axiosCancelRequest = null;

      if (status === STATUS_401_UNAUTHORIZED) {
        return { success: false, data, status };
      }

      if (status === STATUS_200_OK) {
        return { success: true, data, status };
      }
    } catch (error) {
      return handlerErrorAxios(error);
    }
  },

  async [MURREN_SUBSCRIBE]({ commit }, { username }) {
    try {
      const { data, status } = await axios.post(
        `/api/murren/${username}/subscribe/`
      );

      if (status === STATUS_200_OK) {
        const payload = { data, username: username };
        commit(MURREN_PROFILE_CHANGE_SUBSCRIBER_STATUS, payload);
        return { success: true, data };
      }

      return { success: false, data };
    } catch (error) {
      return handlerErrorAxios(error);
    }
  },

  [MURREN_AVATAR_CANCEL_UPLOAD]() {
    if (typeof axiosCancelRequest === "function") {
      axiosCancelRequest();
      axiosCancelRequest = null;
    }
  },

  async [SWITCH_SUBSCRIPTION_NOTIFICATIONS]() {
    try {
      const { status } = await axios.patch(
        "/api/murren/switch_subscription_notifications/"
      );

      if (status === STATUS_204_NO_CONTENT) {
        return { success: true };
      }
    } catch (error) {
      return handlerErrorAxios(error);
    }
  },

  async [SWITCH_MURR_CARD_NOTIFICATIONS]() {
    try {
      const { status } = await axios.patch(
        "/api/murren/switch_murr_card_notifications/"
      );

      if (status === STATUS_204_NO_CONTENT) {
        return { success: true };
      }
    } catch (error) {
      return handlerErrorAxios(error);
    }
  },
};
