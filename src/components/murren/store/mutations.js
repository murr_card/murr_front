import * as type from "./type.js";

export default {
  [type.MURREN_PROFILE_SET](state, murrenData) {
    if (state.profileData) {
      state.profileData = { ...state.profileData, ...murrenData };
    } else {
      state.profileData = murrenData;
    }
  },
  [type.MURREN_PROFILE_CHANGE_SUBSCRIBER_STATUS](state, payload) {
    if (
      !!state.profileData &&
      state.profileData.username === payload.username
    ) {
      state.profileData = { ...state.profileData, ...payload.data };
    }
  },
  [type.MURREN_PROFILE_NOTIFY](state, payload) {
    state.profileData = { ...state.profileData, ...payload };
  },
};
