import axios from "axios";
import { handlerErrorAxios } from "../../../utils/helpres.js";

import {
  STATUS_200_OK,
  STATUS_201_CREATED,
  STATUS_204_NO_CONTENT,
  STATUS_401_UNAUTHORIZED,
} from "../../../utils/http_response_status.js";
import {
  MURR_CARD_BY_USER,
  MURR_CARD_CLEAR,
  MURR_CARD_COVER_RESIZE,
  MURR_CARD_CREATE,
  MURR_CARD_DELETE,
  // MURR_CARD_FETCH_CATEGORIES,
  MURR_CARD_FETCH_LIST,
  MURR_CARD_FETCH_ONE,
  MURR_CARD_LIKED,
  MURR_CARD_MY_LIKES,
  MURR_CARD_MY_SUBSCRIPTIONS,
  MURR_CARD_SET,
  // MURR_CARD_SET_CATEGORIES,
  MURR_CARD_UNLIKED,
  MURR_CARD_UPDATE,
  MURR_CARDS_ADD,
  MURR_CARDS_APPEND,
  MURR_CARDS_UPDATE,
  MURR_CARDS_UPDATE_COMMENT_COUNT,
} from "./type.js";

const MURR_CARDS_START_PAGE = 1;

export default {
  async [MURR_CARD_FETCH_LIST]({ commit }, { page = MURR_CARDS_START_PAGE }) {
    try {
      const { data, status } = await axios.get(`/api/murr_card/?page=${page}`);

      if (status === STATUS_200_OK) {
        commit(MURR_CARDS_APPEND, data);

        return {
          success: true,
          data,
          existsNextPage: data.next !== null,
        };
      }

      return { success: false, data };
    } catch (error) {
      return handlerErrorAxios(error);
    }
  },

  async [MURR_CARD_BY_USER](
    { commit },
    { page = MURR_CARDS_START_PAGE, murrenID }
  ) {
    try {
      const { data, status } = await axios.get(
        `/api/murr_card/?page=${page}&owner=${murrenID}`
      );

      if (status === STATUS_200_OK) {
        commit(MURR_CARDS_APPEND, data);

        return {
          success: true,
          data,
          existsNextPage: data.next !== null,
        };
      }

      return { success: false, data };
    } catch (error) {
      return handlerErrorAxios(error);
    }
  },

  async [MURR_CARD_CREATE]({ commit }, payload) {
    try {
      const formData = new FormData();

      for (let key in payload) {
        if (key === "cover" || key === "coverCropDetail") continue;

        if (payload.hasOwnProperty(key)) {
          const value = payload[key];

          if (typeof value === "object") {
            formData.append(key, JSON.stringify(value));
          } else {
            formData.append(key, value);
          }
        }
      }

      if (payload.cover) {
        /* tmp_image_name.jpeg - PIL requires the name of the file with the extension. Not used anywhere else */
        formData.append("cover", payload.cover, "tmp_image_name.jpeg");
        formData.append(
          "coverCropDetail",
          JSON.stringify(payload.coverCropDetail)
        );
      }

      const { data, status } = await axios.post("/api/murr_card/", formData, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      });

      let result = { success: true, data, status };

      if (status === STATUS_401_UNAUTHORIZED) {
        return { ...result, success: false };
      }

      if (status === STATUS_201_CREATED) {
        commit(MURR_CARDS_ADD, data);

        return result;
      }

      return { ...result, success: false };
    } catch (error) {
      return handlerErrorAxios(error);
    }
  },

  async [MURR_CARD_MY_LIKES]({ commit }, { page = MURR_CARDS_START_PAGE }) {
    try {
      const { data, status } = await axios.get(
        `/api/murr_card/my_likes/?page=${page}`
      );

      if (status === STATUS_200_OK) {
        commit(MURR_CARDS_APPEND, data);

        return {
          success: true,
          data,
          existsNextPage: data.next !== null,
        };
      }

      return { success: false, data };
    } catch (error) {
      return handlerErrorAxios(error);
    }
  },

  async [MURR_CARD_MY_SUBSCRIPTIONS](
    { commit },
    { page = MURR_CARDS_START_PAGE }
  ) {
    try {
      const { data, status } = await axios.get(
        `/api/murr_card/feed/?page=${page}`
      );

      if (status === STATUS_200_OK) {
        commit(MURR_CARDS_APPEND, data);

        return {
          success: true,
          data,
          existsNextPage: data.next !== null,
        };
      }

      return { success: false, data };
    } catch (error) {
      return handlerErrorAxios(error);
    }
  },

  async [MURR_CARD_LIKED](_, { murrID }) {
    try {
      const { data, status } = await axios.post(
        `/api/murr_card/${murrID}/like/`
      );

      return { success: true, status, data };
    } catch (error) {
      return handlerErrorAxios(error);
    }
  },

  async [MURR_CARD_UNLIKED](_, { murrID }) {
    try {
      const { data, status } = await axios.post(
        `/api/murr_card/${murrID}/dislike/`
      );

      return { success: true, status, data };
    } catch (error) {
      return handlerErrorAxios(error);
    }
  },

  async [MURR_CARD_FETCH_ONE]({ commit }, { murrID }) {
    try {
      const { data, status } = await axios.get(`/api/murr_card/${murrID}/`);

      if (status === STATUS_200_OK) {
        commit(MURR_CARD_SET, data);

        return { success: true, data };
      }

      return { success: false, data };
    } catch (error) {
      return handlerErrorAxios(error);
    }
  },

  async [MURR_CARD_DELETE]({ commit }, { murrID }) {
    try {
      const { data, status } = await axios.delete(`/api/murr_card/${murrID}/`);

      if (status === STATUS_204_NO_CONTENT) {
        commit(MURR_CARD_CLEAR);

        return { success: true, message: "Мурр успешно удален!" };
      }

      return { success: false, message: data.detail };
    } catch (error) {
      return handlerErrorAxios(error);
    }
  },

  async [MURR_CARD_COVER_RESIZE](_, { cover }) {
    try {
      const formData = new FormData();
      /* tmp_image_name.jpeg - PIL requires the name of the file with the extension. Not used anywhere else */
      formData.append("cover", cover, "tmp_image_name.jpeg");

      const { status, data } = await axios.post(
        "/api/murr_card/cover_resize/",
        formData,
        {
          responseType: "blob",
          headers: {
            "Content-Type": "multipart/form-data",
          },
        }
      );

      let result = { success: true, data, status };

      if (status === STATUS_401_UNAUTHORIZED) {
        return { ...result, success: false };
      }

      if (status === STATUS_200_OK) {
        return result;
      }

      return { ...result, success: false };
    } catch (error) {
      return handlerErrorAxios(error);
    }
  },

  // async [MURR_CARD_FETCH_CATEGORIES]({ commit }) {
  //   try {

  // const { status, data } = await axios.get("/api/murr_card/category_list/");

  // let result = { success: true, data: data.results, status };
  //
  // if (status === STATUS_200_OK) {
  //   commit(MURR_CARD_SET_CATEGORIES, data.results);
  //
  //   return result;
  // }
  //
  // return { ...result, success: false };
  // } catch (error) {
  //   return handlerErrorAxios(error);
  // }
  // },

  [MURR_CARDS_UPDATE_COMMENT_COUNT]({ commit, getters }) {
    const currentMurrCard = getters.murr;
    const newCommentCount = currentMurrCard.comments_count + 1;
    commit(MURR_CARDS_UPDATE, {
      murrId: currentMurrCard.id,
      updateField: {
        comments_count: newCommentCount,
      },
    });
    commit(MURR_CARD_UPDATE, { comments_count: newCommentCount });
  },
};
