Добавление https:

helm repo add cert-manager https://charts.jetstack.io
helm repo update

применить манифесты cert-issuer.yaml и ингресс


<img src="readme/img/thumbnail.png" align="center" title="Murrengan network"/>

[murrengan.ru](https://murrengan.ru/)

<a href="readme/en"><img src="readme/img/united_states_of_america_usa.png" height="25" width="30" title="English"></a>

### Фронтед часть приложения murr_card

Запускаемся в кубере

# 🌟Поддержать проект🌟

[donationalerts](http://bit.do/eWnnm)

# ❤

## Контакты

[discord](https://discord.gg/gHFtAT3)

[youtube](https://youtube.com/murrengan/)

[telegram](https://t.me/MurrenganChat)

[vk](https://vk.com/murrengan)

[murrengan](https://www.murrengan.ru/)
